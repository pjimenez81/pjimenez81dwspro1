
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="css/Personas.css">
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');

        include_once("Telefono.php");

        function recoge($campo) {
            if (isset($_REQUEST[$campo])) {
                $valor = htmlspecialchars(trim(strip_tags($_REQUEST[$campo])));
            } else {
                $valor = "";
            };
            return $valor;
        }
        
        function grabar($telefono) {
            
            $f = fopen("Telefonos.txt","a");
            $linea = $telefono->getId().";".$telefono->getNumero().";".$telefono->getPersona()."\r\n";
            fwrite($f,$linea);
            fclose($f);
        }
        
        function leer() {
            
            $id = recoge("id");
            $numero = recoge("numero");
            $persona = recoge("telefono");
            
            $telefono = new Telefono($id,$numero,$persona);
            
            return $telefono;
            
        }
        
        
        // RELLENA AUTOMATICAMENTE EL COMBOBOX DEL FORMULARIO DE TELEFONOS CON LOS DATOS DEL FICHERO PERSONAS.
        function combobox() {
            
           
          // $combo ="";
           $f = fopen("Persona.txt", "r");
           while ($datos = fgetcsv($f, 1000, ";")) {
           $numero = count($datos);
                for ($c=0; $c < $numero; $c++) {
                $id = $datos[$c];
                $c++;
                $nombre = $datos[$c];
            
            
                 echo "<option value='".$id."'>".$nombre."</option>";
           
        }
    }
    fclose($f);
  
        }
        
        // MUESTRAS LOS TELEFONOS GRABADOS
        function mostrar() {
          
           $f = fopen("Telefonos.txt", "r");
           while ($datos = fgetcsv($f, 1000, ";")) {
           $numero = count($datos);
                for ($c=0; $c < $numero; $c++) {
                $id = $datos[$c];
                $c++;
                $nombre = $datos[$c];
                $c++;
                $personaid = $datos[$c];
                
                 echo "<tr>";
                 echo "<td>";
                 echo $id;
                 echo "</td>";
                 echo "<td>";
                 echo $nombre;
                 echo "</td>";
                 echo "<td>";
                 echo $personaid;
                 echo "</td>";
                 echo "</tr>";
                 
           
        }
    }
    fclose($f);
  
        }
        
        $telefono = new Telefono("","","");
        $telefono = leer();
        
        if ($telefono->getId() != "" && $telefono->getNumero() != "" && $telefono->getPersona() != "") {
            
            grabar($telefono);
            echo "El teléfono ".$telefono->getNumero()." ha sido grabado.<br>";
            echo "<a id='vol' href='index.php'>Volver</a><br>";
            echo "PROYECTO DWS - Pablo Jiménez Notario - CEEDCV 2016/2017";
        
                    
        } else {
            
            echo "Alguno de los campos esta vacío.";
            echo "<a id='vol' href='index.php'>Volver</a><br>";
           echo "PROYECTO DWS - Pablo Jiménez Notario - CEEDCV 2016/2017";
        }
        
        
        
        ?>
    </body>
</html>
