<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

class Telefono {

//Propiedades
    private $id;
    private $numero;
    private $persona;

//Constructores


    public function __construct($id, $numero, $persona) {
        $this->id = $id;
        $this->numero = $numero;
        $this->persona = $persona;
    }

//Metodos
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNumero() {
        return $this->numero;
    }

    public function setNumero($numero) {
        return $this->numero;
    }

    public function getPersona() {
        return $this->persona;
    }

    public function setPersona($persona) {
        return $this->persona;
    }

}

?>
