
<!DOCTYPE html>
<html>
    <head>
        <title>AGENDA DE TELÉFONOS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/proyecto.css" type="text/css">
    </head>
    <body>
        
        <div class="cabecera">
        <h1>AGENDA DE TELÉFONOS</h1>
        </div>
        <h2>ALTA:</h2>
        <div id="contenedor">
        <div class="botones">
            <p><a href="PersonasFormulario.php"><img src="css/iconos/red.svg"></a></p>
            
            <a href="PersonasFormulario.php">PERSONAS </a>
        
        </div>
        <div class="botones">
            <p><a href="TelefonosFormulario.php"><img src="css/iconos/numero-de-telefono.svg"></a></p>
        <a href="TelefonosFormulario.php">TELÉFONOS</a>
        </div>
        
        <div class="botones">
            <p><a href="DWSProyectoT5Enunciado.pdf"><img src="css/iconos/enunciado.svg"></a></p>
        <a href="DWSProyectoT5Enunciado.pdf">TEMA 5. POO. ENUNCIADO </a>
        </div>
        <div class="botones">
            <p><a href="DWSProyectoDocumentacion.odt"><img src="css/iconos/documentacion.svg"></a></p>
        
            <a href="DWSProyectoDocumentacion.odt">DOCUMENTACIÓN </a>
        </div>
        </div>
        <div id="footer">
        <div>PROYECTO DWS - Pablo Jiménez Notario -  CEEDCV 2016/2017</div>
        <div>Iconos diseñados por <a href="http://www.freepik.com" title="Freepik">Freepik</a> desde <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> con licencia <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
        </div>
        
        

    </body>
</html>
