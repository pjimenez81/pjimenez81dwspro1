
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="css/Personas.css">
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');

        include_once("Persona.php");
        include_once("funciones.php");

        function recoge($campo) {
            if (isset($_REQUEST[$campo])) {
                $valor = htmlspecialchars(trim(strip_tags($_REQUEST[$campo])));
            } else {
                $valor = "";
            };
            return $valor;
        }

        function grabar($persona) {
            $f = fopen("Persona.txt", "a");
            $linea = $persona->getId() . ";" . $persona->getNombre() . "\r\n";
            fwrite($f, $linea);

            fclose($f);
        }

        function leer() {
            $id = recoge("id");
            $nombre = recoge("nombre");
            $perso = new Persona($id, $nombre);
            return $perso;
        }

        $persona = new Persona("", "");
        $persona = leer();
        if ($persona->getId() != "" && $persona->getNombre() != "") {
            grabar($persona);
            echo "Grabado: " . $persona->getNombre() . "<br>";
        } else {
            echo "Error: Campos vacios" . "<br>";
        }
         
        echo "<a id='vol' href='index.php'>Volver</a><br>";
        echo "PROYECTO DWS - Pablo Jiménez Notario - CEEDCV 2016/2017";

        ?>
    </body>
</html>
